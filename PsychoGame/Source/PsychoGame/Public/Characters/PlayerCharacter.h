// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Engine/DataTable.h"
#include "PlayerCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNoParmDelegate);

class UCameraComponent;
class USpringArmComponent;
class UCharacterMovementComponent;

USTRUCT(BlueprintType)
struct FCraftingInfo : public FTableRowBase
{
	GENERATED_BODY()
public:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName CompoonentID;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName ProductID;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bDestroyItemA;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bDestroyItemB;

};

USTRUCT(BlueprintType)
struct FInventoryItem : public FTableRowBase
{
	GENERATED_BODY()

public:
	
	FInventoryItem()
	{
		Name = FText::FromString("Item");
		Action = FText::FromString("Use");
		Description = FText::FromString("Enter description for this item");
		Value = 10;
	}
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	FName ItemID;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<class APickup> ItemPickup;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText Name;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText Action;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Value;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UTexture2D* Thumbnail;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText Description;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FCraftingInfo> CraftingCombinations;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bCanBeUsed;

	//Comparing inventory items for each other
	bool operator==(const FInventoryItem& Item) const
	{
		if (ItemID == Item.ItemID)
			return true;
		else return false;
	}

};


UCLASS()
class PSYCHOGAME_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerCharacter();

	//movement speed
	// bMovement
	// 

	UFUNCTION(BlueprintCallable)
		bool isCrounch();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UCameraComponent* CameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		USpringArmComponent* SpringArmComponent;

	UFUNCTION(BlueprintCallable)
		void Die();

	UFUNCTION(BlueprintCallable)
		void EncreaseWalkSpeed();

	UFUNCTION(BlueprintCallable)
		void DereaseWalkSpeed();

	UFUNCTION(BlueprintCallable)
		void Crounch();


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		int StepSpeed = 10;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TMap<FString, int> CharacterSpeed = { {"Crounch",200},{"Walk", 400},{"Sprint", 600} };

	void MoveForward(float Scale);
	void MoveSide(float Scale);
	void RotateMouse();

public:	

	virtual void Tick(float DeltaTime) override;


	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Settings")
		bool bIsCrounch;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Settings")
		bool bIsMoveble;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character state")
		bool bIsVisible;
	
	UFUNCTION(BlueprintCallable)
		void UseItem();


protected:

	void CheckForInteractables();
};
