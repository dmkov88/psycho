// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseDoor.generated.h"

class UStaticMeshComponent;
class UBoxComponent;
class USceneComponent;

UCLASS()
class PSYCHOGAME_API ABaseDoor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseDoor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	UFUNCTION()
		virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	
	UFUNCTION()
		virtual void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION(BlueprintCallable)
		virtual void DoorOpenAnimation();

	UFUNCTION(BlueprintCallable)
		virtual void DoorCloseAnimation();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Door Setup")
		UStaticMeshComponent* BaseMesh;
private:
	UPROPERTY(EditAnywhere, Category = "Door Setup")
		UBoxComponent* BoxCollider;
	UPROPERTY(EditAnywhere, Category = "Door Setup")
		USceneComponent* DefaultSceneComponent;
};
