// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PoinLight.generated.h"

class USphereComponent;
class UPointLightComponent;

UCLASS()
class PSYCHOGAME_API APoinLight : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APoinLight();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		USphereComponent * SphereCollideLight;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UPointLightComponent * PointLightSource;

	UFUNCTION(BlueprintCallable)
		float PlayerVisibleDistance(APoinLight* Light, APlayerCharacter* Player);

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class APlayerCharacter * PlayerCharacter;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float VisiblePower;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bIsVisible;

	UFUNCTION(BlueprintCallable)
		void CheckDistance(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);
	
	UFUNCTION(BlueprintCallable)
		void EndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	
};
