// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ActorDetectComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNoParDelegate);

class APlayerCharacter;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PSYCHOGAME_API UActorDetectComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UActorDetectComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;


	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		APlayerCharacter * PlayerCharacter;

	UPROPERTY(BlueprintReadOnly)
		bool bIsInsideAnyLight;
		
public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(BlueprintAssignable)
		FNoParDelegate OnLightDetect;

	UPROPERTY(BlueprintReadWrite)
		float LightFeelIntensity;
};
