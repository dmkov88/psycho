// Fill out your copyright notice in the Description page of Project Settings.


#include "PoinLight.h"
#include "Runtime/Engine/Classes/Components/SphereComponent.h"
#include "Runtime/Engine/Classes/Components/PointLightComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "PlayerCharacter.h"

// Sets default values
APoinLight::APoinLight()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PointLightSource = CreateDefaultSubobject<UPointLightComponent>(TEXT("LightSource"));
	PointLightSource->SetupAttachment(RootComponent);

	PointLightSource->bUseInverseSquaredFalloff = false;
	
	SphereCollideLight = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCheckZone"));
	SphereCollideLight->SetupAttachment(PointLightSource);

	SphereCollideLight->OnComponentBeginOverlap.AddDynamic(this, &APoinLight::CheckDistance);
	SphereCollideLight->OnComponentEndOverlap.AddDynamic(this, &APoinLight::EndOverlap);
}

// Called when the game starts or when spawned
void APoinLight::BeginPlay()
{
	Super::BeginPlay();
	SphereCollideLight->SetSphereRadius(PointLightSource->AttenuationRadius);
	PlayerCharacter = Cast<APlayerCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
}

void APoinLight::CheckDistance(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	bIsVisible = true;
	UE_LOG(LogTemp, Warning, TEXT("true"));
}

void APoinLight::EndOverlap(UPrimitiveComponent * OverlappedComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex)
{
	bIsVisible = false;
	UE_LOG(LogTemp, Warning, TEXT("false"));
}

float APoinLight::PlayerVisibleDistance(APoinLight * Light, APlayerCharacter * Player)
{
	if (bIsVisible)
	{
		FVector EyesLocation;
		FRotator EyesRotation;
		Player->GetActorEyesViewPoint(EyesLocation, EyesRotation);

		float Distance = (Light->GetActorLocation() - EyesLocation).Size();

		return Distance;
	}
	else
	{
		return 0;
	}
}

// Called every frame
void APoinLight::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	VisiblePower = pow(PlayerVisibleDistance(this, PlayerCharacter),-1);
	//UE_LOG(LogTemp, Warning, TEXT("%f"), VisiblePower);
}

