// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCharacter.h"
#include "Engine/World.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "ConstructorHelpers.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Interactable.h"
#include "GameplayController.h"


APlayerCharacter::APlayerCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	AutoPossessPlayer = EAutoReceiveInput::Player0;

	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArmComponent->SetupAttachment(RootComponent);

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CharacterCamera"));
	CameraComponent->SetupAttachment(SpringArmComponent);
	//CameraComponent->bUsePawnControlRotation = true;

	if (SpringArmComponent)
	{
		SpringArmComponent->AddLocalTransform(FTransform(FRotator(-80, 0, 0).Quaternion()));
		SpringArmComponent->TargetArmLength = 600;
	}

	/*
	bUseControllerRotationYaw = false;
	SpringArmComponent->bInheritPitch = false;
	SpringArmComponent->bInheritRoll = false;
	SpringArmComponent->bInheritYaw = false;
	*/

	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	GetCharacterMovement()->MaxWalkSpeed = CharacterSpeed["Walk"];
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	SpringArmComponent->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	SpringArmComponent->bUsePawnControlRotation = true;

	CameraComponent->SetupAttachment(SpringArmComponent, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	CameraComponent->bUsePawnControlRotation = false;
}


void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}


void APlayerCharacter::Die()
{
//	PlayAnimMontage();
}

void APlayerCharacter::EncreaseWalkSpeed()
{
	int CurrentWalkSpeed = GetCharacterMovement()->MaxWalkSpeed;
	if (CurrentWalkSpeed < CharacterSpeed["Sprint"])
	{
		if (CurrentWalkSpeed > CharacterSpeed["Crounch"])
		{
			bIsCrounch = false;
		}
		GetCharacterMovement()->MaxWalkSpeed = CharacterSpeed["Walk"];
		GetCharacterMovement()->MaxWalkSpeed += StepSpeed;
	}
}

void APlayerCharacter::DereaseWalkSpeed()
{
	int CurrentWalkSpeed = GetCharacterMovement()->MaxWalkSpeed;
	if (CurrentWalkSpeed > 180)
	{
		if (CurrentWalkSpeed <= CharacterSpeed["Crounch"])
		{
			bIsCrounch = true;
		}
		GetCharacterMovement()->MaxWalkSpeed -= StepSpeed;
	}
}

void APlayerCharacter::Crounch()
{
	bIsCrounch = !bIsCrounch;
	if (bIsCrounch)
	{
		GetCharacterMovement()->MaxWalkSpeed = CharacterSpeed["Crounch"];
	}
	
}

bool APlayerCharacter::isCrounch()
{
	return bIsCrounch;
}

void APlayerCharacter::MoveForward(float Scale)
{
	if ((Controller != NULL) && (Scale != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Scale);
	}
}

void APlayerCharacter::MoveSide(float Scale)
{
	if ((Controller != NULL) && (Scale != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Scale);
	}
}


void APlayerCharacter::RotateMouse()
{
	FHitResult Hit;
	float RotationYaw;

	if (GetWorld()->GetFirstPlayerController()->GetHitResultUnderCursor(ECC_Visibility, false, Hit))
	{
		RotationYaw = FRotationMatrix::MakeFromX(Hit.ImpactPoint - GetActorLocation()).Rotator().Yaw;
	}
	else
	{
		FVector mouseWorldLocation, mouseWorldDirection;

		GetWorld()->GetFirstPlayerController()->DeprojectMousePositionToWorld(mouseWorldLocation, mouseWorldDirection);
		RotationYaw = mouseWorldDirection.Rotation().Yaw;
	}

	SetActorRotation(FRotator(0, RotationYaw, 0));
	
}

void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//RotateMouse();
	CheckForInteractables();
}

void APlayerCharacter::UseItem()
{
}

void APlayerCharacter::CheckForInteractables()
{
	FHitResult HitResult;

	FVector StartTrace = CameraComponent->GetComponentLocation();
	FVector EndTrace = (CameraComponent->GetForwardVector() * 400) + StartTrace;

	FCollisionQueryParams QueryParams;
	QueryParams.AddIgnoredActor(this);

	AGameplayController* Controller = Cast<AGameplayController>(GetController());
	

	if (GetWorld()->LineTraceSingleByChannel(HitResult, StartTrace, EndTrace, ECC_Visibility, QueryParams) && Controller)
	{
		//Check itme "if intracteble"
		if (AInteractable* Interacteble = Cast<AInteractable>(HitResult.GetActor()))
		{
			Controller->CurrentInteractable = Interacteble;
			return;
		}
		
	}

	Controller->CurrentInteractable = nullptr;
}

void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	
	if (!bIsMoveble)
	{
		PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &APlayerCharacter::MoveForward);
		PlayerInputComponent->BindAxis(TEXT("Strafe"), this, &APlayerCharacter::MoveSide);
	}

	PlayerInputComponent->BindAction(TEXT("CharacterSpeedUp"), IE_Pressed, this, &APlayerCharacter::EncreaseWalkSpeed);
	PlayerInputComponent->BindAction(TEXT("CharacterSpeedDown"), IE_Pressed, this, &APlayerCharacter::DereaseWalkSpeed);
	PlayerInputComponent->BindAction(TEXT("Crounch"), IE_Released, this, &APlayerCharacter::Crounch);
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
}


