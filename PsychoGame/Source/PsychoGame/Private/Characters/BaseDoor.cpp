// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseDoor.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"

// Sets default values
ABaseDoor::ABaseDoor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	DefaultSceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultSceneComponent"));
	DefaultSceneComponent->SetupAttachment(RootComponent);

	BaseMesh = CreateAbstractDefaultSubobject<UStaticMeshComponent>(TEXT("DoorMesh"));
	BaseMesh->SetupAttachment(DefaultSceneComponent);

	BoxCollider = CreateAbstractDefaultSubobject<UBoxComponent>(TEXT("CollisionBox"));
	BoxCollider->SetupAttachment(DefaultSceneComponent);

}

// Called when the game starts or when spawned
void ABaseDoor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABaseDoor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABaseDoor::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
}

void ABaseDoor::OnOverlapEnd(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex)
{
}

void ABaseDoor::DoorOpenAnimation()
{
}

void ABaseDoor::DoorCloseAnimation()
{
}

