// Fill out your copyright notice in the Description page of Project Settings.


#include "ActorDetectComponent.h"
#include "Runtime/Engine/Classes/Components/SphereComponent.h"
#include "Runtime/Engine/Classes/Engine/PointLight.h"
#include "Runtime/Engine/Classes/Components/PointLightComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Components/LocalLightComponent.h"
#include "PlayerCharacter.h"

// Sets default values for this component's properties
UActorDetectComponent::UActorDetectComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

}


// Called when the game starts
void UActorDetectComponent::BeginPlay()
{
	PlayerCharacter = Cast<APlayerCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
}


// Called every frame
void UActorDetectComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	bIsInsideAnyLight = false;
	if (bIsInsideAnyLight)
	{
		LightFeelIntensity = 0.f;
	}


	TArray<AActor*> PointLights;
	UGameplayStatics::GetAllActorsOfClass(this, APointLight::StaticClass(), PointLights);

	for (int32 i = 0; i < PointLights.Num(); i++)
	{
		APointLight* Light = Cast<APointLight>(PointLights[i]);
		if (Light)
		{
			ULocalLightComponent* LocalCompLight = Cast<ULocalLightComponent>(Light->GetLightComponent());
			FVector PLayerLocation;
			PLayerLocation = PlayerCharacter->GetActorLocation();
			
			float Distance = (Light->GetActorLocation() - PLayerLocation).Size();
			if (Distance < Light->PointLightComponent->AttenuationRadius)
			{
				FCollisionQueryParams QueryParams;
				QueryParams.AddIgnoredActor(Cast<AActor>(PlayerCharacter));
				QueryParams.bTraceComplex = true;

				FHitResult Hit;
				if (!GetWorld()->LineTraceSingleByChannel(Hit, PLayerLocation, Light->GetActorLocation(), ECC_Visibility, QueryParams))
				{
					bIsInsideAnyLight = true;
					LightFeelIntensity = FMath::GetMappedRangeValueClamped(FVector2D(0.f, LocalCompLight->AttenuationRadius), FVector2D(2.f, 0.f), Distance);
					if (PlayerCharacter->isCrounch())
					{
						LightFeelIntensity *= 0.8;
					}
					break;
				}
			}
		}
	}

	OnLightDetect.Broadcast();
}

